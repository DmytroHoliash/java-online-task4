package com.holiash.logicaltask.arraytask;

import java.util.Arrays;

public class ArrayTaskMain {
  public static void main(String[] args) {
    int[] arr1 = {1, 5, 7, 5, 2, 3};
    int[] arr2 = {8, 1, 7, 2, 5, 5};
    int[] arr = intersection(arr1, arr2);
    for (int el : arr) {
      System.out.print(el + " ");
    }
    System.out.println();
    arr = symmetricDifference(arr1, arr2);
    for (int el : arr) {
      System.out.print(el + " ");
    }
    System.out.println();
  }

  static int[] intersection(int[] arr1, int[] arr2) {
    int[] result = new int[Math.max(arr1.length, arr2.length)];
    int resultIndex = 0;
    for (int i = 0; i < arr1.length; i++) {
      for (int j = 0; j < arr2.length; j++) {
        if (arr2[j] == arr1[i]) {
          result[resultIndex] = arr1[i];
          resultIndex++;
          break;
        }
      }
    }
    result = Arrays.copyOf(result, resultIndex);
    return result;
  }

  static int[] union(int[] arr1, int[] arr2) {
    int[] result = new int[arr1.length + arr2.length];
    int resultIndex = 0;
    for (int i = 0; i < arr1.length; i++) {
      result[resultIndex] = arr1[i];
      resultIndex++;
    }
    for (int i = 0; i < arr2.length; i++) {
      result[resultIndex] = arr2[i];
      resultIndex++;
    }
    return result;
  }

  static int[] symmetricDifference(int[] arr1, int[] arr2) {
    int[] union = union(arr1, arr2);
    int[] intersect = intersection(arr1, arr2);
    int[] result = new int[union.length];
    int resultIndex = 0;
    boolean check;
    for (int i = 0; i < union.length; i++) {
      check = true;
      for (int j = 0; j < intersect.length; j++) {
        if (union[i] == intersect[j]) {
          check = false;
          break;
        }
      }
      if (check) {
        result[resultIndex] = union[i];
        resultIndex++;
      }
    }
    result = Arrays.copyOf(result, resultIndex);
    return result;
  }

}
