package com.holiash.collectionstask.comparableclass.countrycapitalgenerator;

import java.util.HashMap;
import java.util.Map;

public class CountryCapitalStorage {
  private static Map<String, String> countryCapitalMap;

  static {
    countryCapitalMap = new HashMap<String, String>();
    countryCapitalMap.put("Ukraine", "Kyiv");
    countryCapitalMap.put("Poland", "Warsaw");
    countryCapitalMap.put("Slovakia", "Bratislava");
    countryCapitalMap.put("Belarus", "Minsk");
    countryCapitalMap.put("Hungary", "Budapest");
    countryCapitalMap.put("Romania", "Bucharest");
    countryCapitalMap.put("Russia", "Moscow");
    countryCapitalMap.put("France", "Paris");
    countryCapitalMap.put("Austria", "Vienna");
    countryCapitalMap.put("Germany", "Berlin");
    countryCapitalMap.put("Finland", "Helsinki");
    countryCapitalMap.put("Netherlands", "Amsterdam");
    countryCapitalMap.put("Spain", "Madrid");
  }

  public static void add(String country, String capital){
    countryCapitalMap.put(country, capital);
  }

  public static void remove(String country) {
    countryCapitalMap.remove(country);
  }

  public static Map<String, String> getCountryCapitalMap(){
    return countryCapitalMap;
  }
}
