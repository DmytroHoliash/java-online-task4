package com.holiash.collectionstask.comparableclass.countrycapitalgenerator;

import com.holiash.collectionstask.comparableclass.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class CountryCapitalGenerator {
  private Map<String, String> countryCapital;
  private Random randIndex;
  private int numberOfCountries;

  public CountryCapitalGenerator() {
    this.countryCapital = CountryCapitalStorage.getCountryCapitalMap();
    randIndex = new Random();
    this.numberOfCountries = this.countryCapital.size();
  }

  public List<Pair<String, String>> generateCountryCapital(int listLength) {
    Object[] countries = this.countryCapital.keySet().toArray();
    List<Pair<String, String>> generated = new ArrayList<Pair<String, String>>();
    int index;
    String country;
    for (int i = 0; i < listLength; i++) {
      index = randIndex.nextInt(this.numberOfCountries);
      country = (String) countries[index];
      generated.add(new Pair<String, String>(country, this.countryCapital.get(country)));
    }
    return generated;
  }
}
