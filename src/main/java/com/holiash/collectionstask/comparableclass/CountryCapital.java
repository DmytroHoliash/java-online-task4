package com.holiash.collectionstask.comparableclass;

public class CountryCapital implements Comparable<CountryCapital> {
  private String country;
  private String capital;

  public CountryCapital(String country, String capital){
    this.country = country;
    this.capital = capital;
  }

  public String getCountry() {
    return country;
  }

  public String getCapital() {
    return capital;
  }

  @Override
  public int compareTo(CountryCapital o) {
    return this.country.compareTo(o.getCountry());
  }

  @Override
  public String toString() {
    return this.country + "=" + this.capital;
  }
}
