package com.holiash.collectionstask.comparableclass;

import com.holiash.collectionstask.comparableclass.countrycapitalgenerator.CountryCapitalGenerator;

import java.util.ArrayList;
import java.util.List;

public class ComparableMain {
  public static void main(String[] args) {
    CountryCapitalGenerator generator = new CountryCapitalGenerator();
    List<Pair<String, String>> generatedCountryCapitals = generator.generateCountryCapital(20);
    List<CountryCapital> sortedCountryCapitals = new ArrayList<CountryCapital>();
    for (Pair<String, String> countryCapital : generatedCountryCapitals) {
      sortedCountryCapitals.add(new CountryCapital(countryCapital.getKey(), countryCapital.getValue()));
    }
    sortedCountryCapitals.sort(CountryCapital::compareTo);
    System.out.println("Sorted by country using Comparable interface: ");
    System.out.println(sortedCountryCapitals);
    CountryCapitalComparator comparator = new CountryCapitalComparator();
    System.out.println("---------------------------------------------");
    sortedCountryCapitals.sort(comparator);
    System.out.println("Sorted by capital using Comparator interface: ");
    System.out.println(sortedCountryCapitals);
  }

}
