package com.holiash.collectionstask.stringscontainer;

import java.util.ArrayList;
import java.util.List;

public class StringsContainerMain {

  /**
   * Testing adding and removing of 1000
   * elements from StringContainer
   *
   * @return time in nanoseconds
   */
  static long timeForStringContainer() {
    StringContainer sc = new StringContainer();
    long start, finish;
    start = System.nanoTime();
    for (int i = 0; i < 1000; i++) {
      sc.add("TEXT");
    }
    for (int i = 0; i < 1000; i++) {
      sc.remove();
    }
    finish = System.nanoTime();
    return finish - start;
  }

  /**
   * Testing adding and removing of 1000
   * elements from ArrayList
   *
   * @return time in nanoseconds
   */
  static long timeForArrayList() {
    List<String> list = new ArrayList<String>();
    long start, finish;
    start = System.nanoTime();
    for (int i = 0; i < 1000; i++) {
      list.add("TEXT");
    }
    for (int i = 0; i < 1000; i++) {
      list.remove(list.size() - 1);
    }
    finish = System.nanoTime();
    return finish - start;
  }

  public static void main(String[] args) {
    System.out.println("cold start time for my container " + String.format("%,12d", timeForStringContainer()) + " ns");
    System.out.println("warmed JRE time for my container" + String.format("%,12d", timeForStringContainer()) + " ns");
    System.out.println("cold start time for ArrayList" + String.format("%,12d", timeForArrayList()) + " ns");
    System.out.println("warmed JRE time for ArrayList" + String.format("%,12d", timeForArrayList()) + " ns");
  }
}
