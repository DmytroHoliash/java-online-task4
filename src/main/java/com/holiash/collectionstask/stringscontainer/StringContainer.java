package com.holiash.collectionstask.stringscontainer;

import java.util.Arrays;

public class StringContainer {
  private String[] array;
  private int length;
  private int size;

  public StringContainer() {
    this(10);
  }

  public StringContainer(int initialCapacity) {
    this.size = initialCapacity;
    this.length = 0;
    this.array = new String[initialCapacity];
  }

  public StringContainer(String[] array) {
    this.size = (int) (array.length * 1.5 + 1);
    this.length = array.length;
    this.array = array;
  }

  private void sizeUp() {
    this.size = (int) (this.size * 1.5 + 1);
    this.array = Arrays.copyOf(this.array, this.size);
  }

  private void sizeDown() {
    this.size /= 2;
    this.array = Arrays.copyOf(this.array, this.size);
  }

  public void add(String element) {
    if (this.length == this.size) {
      this.sizeUp();
    }
    this.array[length] = element;
    length++;
  }

  public void remove() {
    if (length == 0) {
      throw new EmptyListException("List is empty. Nothing to remove.");
    }
    if (this.size / 2 > 10) {
      if (length < size / 2) {
        this.sizeDown();
      }
    }
    this.array[length] = null;
    length--;
  }

  public String get(int index) {
    if (index < 0 || index >= this.length) {
      throw new IndexOutOfBoundsException();
    }
    return this.array[index];
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("[ ");
    for (int i = 0; i < this.length; i++) {
      sb.append("\"").append(this.array[i]).append("\" ");
    }
    sb.append("]");
    return sb.toString();
  }
}
